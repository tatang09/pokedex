const BULBASAUR = Cypress.env("bulbasaur");

describe("renders the pokedex page", () => {

    beforeEach(() => {
        cy.visit("/");
    });

    it("renders correctly", () => {
        cy.get(".grid-container").should("exist");  
    });

    it("it renders some child component", () => {
       cy.findAllByText(BULBASAUR).should("exist");  
       cy.findAllByText("#001").should("exist"); 
       cy.findAllByText("Poison").should("exist"); 
    });
});