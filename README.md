### To run the project

Yarn install after pulling the repository

### To run test command (Cypress)

1) Yarn start
2) On separate terminal run 'npm test' or 'yarn cypress open-ct' for component testing
    a) Component testing
    b) Mock API call
    c) e2e testing

   -Popup window will appear, you can select from the .js file to run your test


### Questtions

1) How did you decide on the technical and architectural choices used as part of your solution?
    - technical side is base on the tech stack that I am currently using, as of the architectural part not much since the app is smaall.

2) Are there any improvements you could make to your submission?
    - Yes, folder structuring, app responsiveness, more testings..

3) What would you do differently if you were allocated more time?
    - Make actual api calls from sample endpoints where I can use more react hooks.
    - Add more page to include navigations.
    - Add additional testings. (I'm newbie to cypress)
    - Improve responsivenss (I'm not good at css)