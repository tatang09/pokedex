export const pokemonTypes = {
    GRASS: "Grass",
    POISON: "Poison",
    WATER: "Water",
    BUG: "Bug",
    FIRE: "Fire",
    ELECTRIC: "Electric"
}

export const typeColors = {
    GRASS: "#81C784",
    POISON: "#CE93D8",
    WATER: "#0D47A1",
    BUG: "#7CB342",
    FIRE: "#F57C00",
    ELECTRIC: "#FFA726"
}