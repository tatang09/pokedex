import { React } from 'react';
 
import './Pages.css';

const LoadingPage = () => {

    return (

        <div>
            <p className={`loading`}>
                {`Catching them all...`}
            </p>
        </div>

    )
}


export default LoadingPage;