import { React } from 'react';
 
import './Pages.css';

const ErrorPage = () => {
    return (
        <div>
            <p className={`error`}>
                {`Something went wrong. We weren't able to find any pokemon`}
            </p>
        </div>
    )
}

export default ErrorPage;