import { React } from 'react';
import Card from '../components/_base/Card';
import './Pages.css';

const Pokedex = ({ data }) => {

    const mapPokemonData = () => {
        return data?.map(pokemon => {
            return <Card key={pokemon.number} pokemonInfo={pokemon} />
        });
    }

    return (
        <div className="grid-container" >
            {mapPokemonData()}
        </div>
    )
}


export default Pokedex;