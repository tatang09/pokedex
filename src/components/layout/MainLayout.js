import { React } from 'react';
import Header from './Header';
import Footer from './Footer';


import './Layout.css';

const MainLayout = ({ children }) => {
    return (
        <div className={`container`}>
           <Header />
            <div id="content">
                {children}
            </div>
            <Footer />
        </div>
    )
}


export default MainLayout;

 