import { React } from 'react';
import './Layout.css';

const Header = () => {

    const headerText = "The Online Pokedex";

    return (
        <header>
            <div>
                <h2 id={`header-title`}>
                    {headerText}
                </h2>
            </div>
        </header>
    )
}


export default Header