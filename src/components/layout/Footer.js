import { React } from 'react';
import './Layout.css';

const Footer = () => {

    const footerText1 = "Copyright \u00A9 2020 React Code Challenge. All Rights Reserved.";
    const footerText2 = "Use of pokemon images under fair use.";
    const footerText3 = "Not for sale redistribution.";

    return (
        <footer>
            <div>
                <span>{footerText1}</span>
                <span>{footerText2}</span>
                <span>{footerText3}</span>
            </div>
        </footer>
    )
}


export default Footer