import { React } from 'react';
import Image from './Image';
import Type from './Type';

import './Components.css';

const Card = ({ pokemonInfo }) => {

    const { number, types, name } = pokemonInfo;

    return (
        <div
            key={number}
             className={`card-container`}
        >
            <div className={`card`}>
                <Image pokemon={pokemonInfo} />
                <div className={`card-details`}>
                    <h2>
                        {`#${number}`}
                    </h2>
                    <span className={`pokemon-name`}>
                        {name}
                    </span>
                </div>
                <Type types={types} />
            </div>
        </div>
    )
}


export default Card;