import { React } from 'react';

import './Components.css';

const Image = ({ pokemon }) => {

    const { image, name } = pokemon;

    const { url, width, height } = image;

    return (
        <img
            className={`pokemon-image`}
            width={width}
            height={height}
            alt={name}
            src={`/${url}`} />
    )
}


export default Image;