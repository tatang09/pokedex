import { React } from 'react';
import { pokemonTypes, typeColors } from '../../helper/helper';


import './Components.css';

const Type = ({ types }) => {

    const typeColor = (type) => {

        let typeColor = "transparent";

        switch (type) {
            case pokemonTypes.GRASS:
                typeColor = typeColors.GRASS
                break;
            case pokemonTypes.POISON:
                typeColor = typeColors.POISON
                break;
            case pokemonTypes.BUG:
                typeColor = typeColors.BUG
                break;
            case pokemonTypes.FIRE:
                typeColor = typeColors.FIRE
                break;
            case pokemonTypes.WATER:
                typeColor = typeColors.WATER
                break;
            case pokemonTypes.ELECTRIC:
                typeColor = typeColors.ELECTRIC
                break;
            default:
                break;
        }

        return typeColor;
    }

    return (
        <div className={`pokemon-type-container`} >
            {
                types.map((type, index) => {
                    return <span
                        key={index} style={{
                            backgroundColor: typeColor(type)
                        }}>{type}</span>
                })
            }
        </div>
    )
}


export default Type;