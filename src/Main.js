import { React, useState, useEffect } from 'react';
import Pokedex from '../src/pages/Pokedex';
import ErrorPage from './pages/ErrorPage';
import LoadingPage from '../src/pages/LoadingPage';
import MainLayout from '../src/components/layout/MainLayout';
import { fetchData } from './data/api';

const Main = () => {

    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([]);

    useEffect(() => {
        apiCall();
    }, []);

    useEffect(() => {
        setLoading(false);
    }, [data]);

    const apiCall = async () => {
        setLoading(true);
        const { total, entries } = await fetchData();
        setData(entries);
    }

    return (
        <MainLayout>
            {
                loading ? <LoadingPage /> : <Pokedex data={data} />
            }
        </MainLayout>
    )
}


export default Main;