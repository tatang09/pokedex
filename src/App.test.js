
/* global cy */
import React from 'react';
import { mount } from '@cypress/react';
import App from './App';
import ErrorPage from './pages/ErrorPage';

it('renders The Online Pokedex text', () => {
  mount(<App />);
  cy.get('h2').contains('The Online Pokedex');
});

it('renders the Error page', () => {
  mount(<ErrorPage />);
  cy.get('.error').contains('Something went wrong.');
});